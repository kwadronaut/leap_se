@title = 'LEAP Platform 0.10.0 released!'
@author = 'micah'
@posted_at = '2017-11-28'
@more = true
@preview_image = '/img/SBX-1.jpg'

Platform 0.10.0 has been released! This release was made to update of all client-side daemons to newest releases, like Soledad and OpenVPN. 

This introduces a *compatibility change*: by setting the platform version to 0.10, it also requires client 0.9.4 or later. We also switched the development branch to the 'master' branch and are creating a branch called 0.10.x to push hot-fixes during the 0.10 life-cycle.

Note: This will be the last major release of the LEAP Platform for Debian Jessie. We will continue to support 0.10 with minor releases with important security and bug fixes, but the next major release will require an upgrade to Stretch.

We have both [[upgrade => platform/upgrading/upgrade-0-10]] and [[install => platform/tutorials/quick-start]] instructions available.

New Features:

* Tor single-hop onion service capability.
* `leap info` is now run after deploy
* Timestamps are added to deployments
* Missing ssh host keys are generated on node init
* Private networking support for local Vagrant development
* Static sites get lets encrypt support
* add command `leap node disable`, `leap node enable`, `leap ping`

Notable Changes:

* Removed haproxy because we don't support multi-node couchdb installations anymore (#8144).
* Disable nagios notification emails (#8772).
* Fix layout of apt repository (#8888)
* Limit what archive signing keys are accepted for the leap debian repository packages (#8425).
* Monitor the Webapp logs for errors (#5174).
* Moved development to the master branch.
* Rewrite leap_cli ssh code
* Debian wheezy was fully deprecated
* Restructure package archives to enable auto packaging, and CI testing
* Significant CI improvements
* Troubleshooting information added to `leap user ls`
* Couchdb service is no longer required on soledad nodes (#8693)
* Tor service refactored (#8864), and v3 hidden service support added (#8879)
* Fixed unattended-upgrades (#8891)
* Alert on 409 responses for webapp
* Many other issues resolved, [full list:](https://0xacab.org/groups/leap/milestones/platform-010?title=Platform+0.10)

Includes:

* leap_web: 0.9.2
* nickserver: 0.10.0
* leap-mx: 0.10.1
* soledad-server: 0.10.5

[Commits](https://0xacab.org/groups/leap/milestones/platform-010?title=Platform+0.10)

For details on about all the changes included in this release please consult the
[LEAP platform 0.10 milestone](https://0xacab.org/leap/platform/milestones/7 ).
